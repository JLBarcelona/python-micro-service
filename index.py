import pandas as pd
import openpyxl
# Replace 'your_file.xlsx' with the actual path to your Excel file
excel_file_path = 'template.xlsx'

# Read the Excel file into a pandas DataFrame
df = pd.read_excel(excel_file_path)

array_replace = ["VARIABLE_A", "VARIABLE_B", "VARIABLE_C", "VARIABLE_D"]

df['Unnamed: 1'].replace({'VARIABLE_A': 'Edited A'}, inplace=True)
df['Unnamed: 2'].replace({'VARIABLE_B': 'Edited B'}, inplace=True)
df['Unnamed: 3'].replace({'VARIABLE_C': 'Edited C'}, inplace=True)
df['Unnamed: 4'].replace({'VARIABLE_D': 'Edited D'}, inplace=True)

# Display the DataFrame (optional)
# print(df)

# output_file_path = 'modified_file.xlsx'
# df.to_excel(output_file_path, index=False)

book = openpyxl.load_workbook(excel_file_path)

# Open the same sheet as the original Excel file
writer = pd.ExcelWriter(excel_file_path, engine='openpyxl')
writer.book = book
writer.sheets = {ws.title: ws for ws in book.worksheets}

# Write the modified DataFrame to the same sheet in the Excel file
df.to_excel(writer, index=False)

# Save the Excel file
writer.save()
